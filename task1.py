#!/usr/bin/python3

def numWay(n):
    a = 0
    b = 1
    for i in range(n):
        c = a+b
        a = b
        b = c
    return b

if __name__ == "__main__":
    print("numWay(7) =", numWay(7))


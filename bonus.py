#!/usr/bin/python3

def gen(n, s):
    counter = [0]
    while len(counter) < n:
        yield counter
        for i in range(len(counter)):
            counter[i] = (counter[i]+1)%(s)
            if counter[i] != 0: break
        if sum(counter) == 0: counter = counter + [0]
    yield [0]*n


def numWay(n, l):
    for config in gen(n, len(l)):
        # check if we have a valid configuration
        nSteps = sum([l[i] for i in config])
        if nSteps == n:
            yield [l[i] for i in config]


if __name__ == "__main__":
    print("numWay(7, [1, 2])")
    for f in numWay(7, [1, 2]):
        print(f)

    print("numWay(4, [1, 3, 5])")
    for f in numWay(4, [1, 3, 5]):
        print(f)


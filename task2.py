#!/usr/bin/python3

def numWay(n, l):
    rec = [0] * (max(l) + 1)
    rec[0] = 1
    for i in range(n):
        rec = [0] + rec[0:-1]  # shift by 1
        rec[0] = sum([rec[x] for x in l])
    return rec[0]

if __name__ == "__main__":
    print("numWay(4, [1, 3, 5]) =", numWay(4, [1, 3, 5]))
    print("numWay(7, [1, 2]) = ", numWay(7, [1, 2]))

